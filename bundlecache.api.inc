<?php

/**
 * @file
 * Contains generic ways of manipulating the configuration of bundlecache module.
 */

// Define indexes for the order of the file in the Bundlecache_File::query() function below.
define('BUNDLECACHE_FILE_PATH', 0);
define('BUNDLECACHE_FILE_SIZE', 1);
define('BUNDLECACHE_FILE_HASH', 2);
define('BUNDLECACHE_FILE_KIND', 3);
define('BUNDLECACHE_FILE_FID', 4);


function bundlecache_calculate_bundles($kind, $files) {
  $available_bundles = bundlecache_get_bundles($kind);
  $files = bundlecache_get_file_sizes($kind, $files);
  $chosen_bundles = array();

  while (count($files) > 0 && $bid = bundlecache_pick_cheapest_bundle($files, $available_bundles)) {
    // Remove all files contained by the chosen bundle.
    $files = array_diff_key($files, array_flip($available_bundles[$bid]->committed_files));
    // Remove the bundle from the list of available bundles and flatten it so that
    // it only retains the required information.
    $chosen_bundles[$available_bundles[$bid]->url()] = $available_bundles[$bid]->committed_files;
    unset($available_bundles[$bid]);
  }

  return $chosen_bundles;
}

function bundlecache_get_bundles($kind) {
  $bundles = &drupal_static(__FUNCTION__, array());

  if (!isset($bundles[$kind])) {
    $bundles[$kind] = cache_get("$kind:bundles", 'cache_bundlecache');
    if ($bundles[$kind] === FALSE) {
      // Once http://drupal.org/node/742042 is in, use this code instead:
      //$bundles[$kind] = Bundlecache_Bundle::query()->condition('b.kind', $kind)->execute()->fetchAllAssoc('path');
      $result = Bundlecache_Bundle::query()->condition('b.kind', $kind)->condition('b.enabled', TRUE)->execute();
      $bundles[$kind] = array();
      foreach ($result as $bundle) {
        $bundle->init();
        $bundles[$kind][$bundle->path] = $bundle;
      }
      cache_set("$kind:bundles", $bundles[$kind], 'cache_bundlecache');
    }
    else {
      $bundles[$kind] = $bundles[$kind]->data;
    }
  }

  return $bundles[$kind];
}

function bundlecache_get_file_sizes($kind, $files) {
  $sizes = &drupal_static(__FUNCTION__, array());

  if (!isset($sizes[$kind])) {
    $sizes[$kind] = cache_get("$kind:file_sizes", 'cache_bundlecache');
    if ($sizes[$kind] === FALSE) {
      $result = Bundlecache_File::query()->condition('f.kind', $kind)->execute();
      $sizes[$kind] = array();
      foreach ($result as $file) {
        $file->init();
        $sizes[$kind][$file->path] = $file->size;
      }
      cache_set("$kind:file_sizes", $sizes[$kind], 'cache_bundlecache');
    }
    else {
      $sizes[$kind] = $sizes[$kind]->data;
    }
  }

  return array_intersect_key($sizes[$kind], array_flip($files));
}

function bundlecache_pick_cheapest_bundle($files, $bundles) {
  $penalty = variable_get('bundlecache_extra_request_penalty', 10000);
  $total_cost = array_sum($files) + count($files) * $penalty;
  $cheapest_cost = $total_cost;
  $cheapest_bundle = FALSE;

  // Try adding a bundle and calculate the savings. If the total size is lower,
  // use it as new baseline and store the bundle ID.
  foreach ($bundles as $bid => $bundle) {
    $replaced_files = array_intersect_key($files, array_flip($bundle->committed_files));
    $bundle_cost = $total_cost + $bundle->size + $penalty;
    $bundle_cost -= array_sum($replaced_files) + count($replaced_files) * $penalty;

    if ($bundle_cost < $cheapest_cost) {
      $cheapest_cost = $bundle_cost;
      $cheapest_bundle = $bid;
    }
  }

  return $cheapest_bundle;
}

function bundlecache_update_bundles() {
  $result = Bundlecache_Bundle::query()->condition('dirty', TRUE)->execute()->fetchAll();
  $bundles = array();

  foreach ($result as $bundle) {
    set_time_limit(30);
    $bundle->init();
    $bundle->aggregate();
    $bundle->save();
    $bundles[] = $bundle;
  }

  cache_clear_all('*', 'cache_bundlecache', TRUE);
  return $bundles;
}

/**
 * Searches for new support files and adds them to the database.
 */
function bundlecache_detect_files() {
  $existing = bundlecache_file_listing();

  $result = Bundlecache_File::query()->execute();
  $known = array();
  foreach ($result as $file) {
    $file->init();
    $known[$file->path] = $file->hash;
  }

  $new = array_diff_key($existing, $known);
  $updated = array_diff_assoc(array_intersect_key($existing, $known), $known);
  $deleted = array_diff_key($known, $existing);

  array_walk($new, array('Bundlecache_File', 'createRecord'));
  array_walk($updated, array('Bundlecache_File', 'updateRecord'));
  array_walk($deleted, array('Bundlecache_File', 'deleteRecord'));

  return array($new, $updated, $deleted);
}

/**
 * Returns a list of certain files in certain directories.
 *
 * @return
 *   An array of arrays containing information on the found files.
 */
function bundlecache_file_listing() {
  global $conf;
  $config = conf_path();

  $search_dirs =  array(
    'misc',
    'modules',
    'themes',
    'sites/all/modules',
    'sites/all/themes',
    'sites/all/libraries',
    'profiles/' . $conf['install_profile'] . '/modules',
    'profiles/' . $conf['install_profile'] . '/themes',
    'profiles/' . $conf['install_profile'] . '/libraries',
     $config . '/modules',
     $config . '/themes',
     $config . '/libraries',
  );

  $files = array();
  foreach ($search_dirs as $dir) {
    if (count($keys = array_keys(file_scan_directory($dir, '/\.(css|js)$/')))) {
      // Calculate the MD5 sum for each file.
      $files += array_combine($keys, array_map('md5_file', $keys));
    }
  }

  return $files;
}

function bundlecache_batch_create($data) {
  $ignored = array();
  $names = array();

  foreach ($data as $kind => $bundles) {
    foreach ($bundles as $title => $files) {
      $title = bundlecache_ui_find_unique_name($kind, $title);
      $bundle = Bundlecache_Bundle::createRecord(array('title' => $title, 'kind' => $kind));

      $added = array();
      foreach (Bundlecache_File::query()->condition('f.path', $files, 'IN')->execute() as $file) {
        $bundle->addFile($file);
        $added[] = $file->path;
      }
      $bundle->save();

      $ignored = array_merge($ignored, array_diff($files, $added));
      $names[] = l($bundle->title, 'admin/config/development/bundlecache/bundle/' . $bundle->bid . '/edit');
    }
  }

  return array($ignored, $names);
}

function bundlecache_force_recreation() {
  db_query('UPDATE {bundlecache_bundle} SET dirty = 1');
}

function bundlecache_minify_js($path) {
  if (!file_exists($path)) {
    throw new Exception(t('The file %file doesn\'t exist. You should detect files again to make sure the cache is in sync.'));
  }

  $directory = 'bundlecache://minified';
  file_prepare_directory($directory, FILE_CREATE_DIRECTORY);
  $cache_path = $directory . '/' . md5_file($path) . '.' . pathinfo($path, PATHINFO_EXTENSION);

  if (!file_exists($cache_path)) {
    module_load_include('php', 'bundlecache', 'jsmin');
    $minified = JSMin::minify(file_get_contents($path));
    file_unmanaged_save_data($minified, $cache_path, FILE_EXISTS_REPLACE);
    return $minified;
  }
  else {
    return file_get_contents($cache_path);
  }
}

function bundlecache_clear_caches() {
  cache_clear_all('*', 'cache_bundlecache', TRUE);
}

class Bundlecache_Bundle {
  public $bid = NULL;
  public $kind = 'css';
  public $title = '';
  public $programmatic = FALSE;
  public $enabled = TRUE;
  public $path = '';
  public $size = 0;
  public $dirty = FALSE;
  public $files = array();
  public $committed_files = array();
  protected $original_values = array();


  public static function query() {
    $query = db_select('bundlecache_bundle', 'b', array('fetch' => 'Bundlecache_Bundle'));
    $query->fields('b');
    return $query;
  }

  public static function createRecord($options) {
    $bundle = new Bundlecache_Bundle();
    return $bundle->create($options);
  }

  public function init() {
    if (isset($this->bid)) {
      $this->bid = (int)$this->bid;
      $this->programmatic = (bool)$this->programmatic;
      $this->enabled = (bool)$this->enabled;
      $this->size = (int)$this->size;
      $this->dirty = (bool)$this->dirty;
      $this->files = db_query('SELECT f.path FROM {bundlecache_file_bundle} fb LEFT JOIN {bundlecache_file} f ON fb.fid = f.fid WHERE fb.bid = :bid ORDER BY fb.weight', array(':bid' => $this->bid))->fetchCol();
      $this->committed_files = unserialize($this->committed_files);
      if (!is_array($this->committed_files)) {
        $this->committed_files = array();
      }

      $this->original_values = $this->fields();
    }
  }

  public function url() {
    if ($this->path) {
      return file_create_url($this->path);
    }
    else {
      return FALSE;
    }
  }

  public function updatePath() {
    $directory = 'bundlecache://' . $this->kind;
    file_prepare_directory($directory, FILE_CREATE_DIRECTORY);
    $hash = substr(base_convert(md5($this->contents), 16, 36), 0, 8);
    $title = $this->programmatic ? pathinfo($this->title, PATHINFO_FILENAME) : $this->title;
    $title = preg_replace('/([_-]* +[_-]*|[_-]{2,})/', '-', drupal_strtolower($title));
    $title = preg_replace('/[^a-z0-9\-_.]/', '', $title);
    $title = preg_replace('/^ad/', "{$this->kind}_ad", $title);
    $this->path = "{$directory}/{$title}_{$hash}.{$this->kind}";
  }

  public function create($options) {
    foreach ($options as $key => $value) {
      $this->$key = $value;
    }
    $this->bid = db_insert('bundlecache_bundle')->fields($this->fields())->execute();
    return $this;
  }

  public function delete() {
    db_delete('bundlecache_file_bundle')->condition('bid', $this->bid)->execute();
    db_delete('bundlecache_bundle')->condition('bid', $this->bid)->execute();
  }

  // Original function: drupal_build_css_cache()
  protected function aggregateCSS() {
    $contents = '';

    $base = base_path();
    # $base = variable_get('bundlecache_graphics_server', '');
    # if ($base === '') {
    #   $base = base_path();
    # }

    // Build aggregate CSS file.
    foreach ($this->files as $path) {
      // Only 'file' stylesheets can be aggregated.
      $content = drupal_load_stylesheet($path, TRUE);
      // Return the path to where this CSS file originated from.
      _drupal_build_css_path(NULL, $base . dirname($path) . '/');
      // Prefix all paths within this CSS file, ignoring external and absolute paths.
      $contents .= preg_replace_callback('/url\([\'"]?(?![a-z]+:|\/+)([^\'")]+)[\'"]?\)/i', '_drupal_build_css_path', $content);
    }

    // Per the W3C specification at http://www.w3.org/TR/REC-CSS2/cascade.html#at-import,
    // @import rules must proceed any other style, so we move those to the top.
    $regexp = '/@import[^;]+;/i';
    preg_match_all($regexp, $contents, $matches);
    $this->contents = implode('', $matches[0]) . preg_replace($regexp, '', $contents);
  }

  // Original function: drupal_build_js_cache()
  protected function aggregateJS() {
    $contents = array();

    // Build aggregate JS file.
    foreach ($this->files as $path) {
      try {
        $contents[] = bundlecache_minify_js($path);
      }
      catch (JSMinException $e) {
        // Add the path information so that we know which file failed and rethrow it.
        $e->path = $path;
        throw $e;
      }
    }

    // Append a ';' after each JS file to prevent them from running together.
    $this->contents = ';' . implode(";\n", $contents);
  }

  public function aggregate() {
    try {
      if ($this->kind == 'css') {
        $this->aggregateCSS();
      }
      elseif ($this->kind == 'js') {
        $this->aggregateJS();
      }

      # if ($this->programmatic) {
      #   $this->contents = '/*' . implode('', $this->files) . "*/\n" . $this->contents;
      # }

      $this->updatePath();
      if (file_unmanaged_save_data($this->contents, $this->path, FILE_EXISTS_REPLACE) === FALSE) {
        throw new Exception(t('Error while saving file %file.', array('%file' => $this->path)));
      }
      $this->size = strlen(gzencode($this->contents));
      $this->committed_files = $this->files;
      $this->dirty = FALSE;
    }
    catch (JSMinException $e) {
      watchdog('bundlecache', 'Rebuilding the bundle %title failed because minification for %file failed: <pre>@return</pre>', array('%title' => $this->title, '%file' => $e->path, '@return' => $e->getMessage()), WATCHDOG_WARNING);
      $e->title = $this->title;
      throw $e;
    }
    catch (Exception $e) {
      watchdog('bundlecache', 'Rebuilding the bundle %title failed: <pre>@return</pre>', array('%title' => $this->title, '@return' => $e->getMessage()), WATCHDOG_WARNING);
      $e->title = $this->title;
      throw $e;
    }
  }

  public function fields() {
    return array(
      'path' => $this->path,
      'size' => $this->size,
      'kind' => $this->kind,
      'title' => $this->title,
      'programmatic' => (int)$this->programmatic,
      'enabled' => (int)$this->enabled,
      'dirty' => (int)$this->dirty,
      'committed_files' => serialize($this->committed_files),
    );
  }

  public function changed() {
    return (bool)count(array_diff_assoc($this->fields(), $this->original_values));
  }

  public function save() {
    if ($this->changed()) {
      db_update('bundlecache_bundle')->condition('bid', $this->bid)->fields($this->fields())->execute();
    }
    if (isset($this->removeFileQuery)) {
      $this->removeFileQuery->execute();
    }
    if (isset($this->addFileQuery)) {
      $this->addFileQuery->execute();
    }

    $this->original_values = $this->fields();
    unset($this->removeFileQuery);
    unset($this->addFileQuery);
  }

  protected $addFileQuery = NULL;
  public function addFile(Bundlecache_File $file) {
    static $weight;
    if (!in_array($file->path, $this->files)) {
      if (!isset($this->addFileQuery)) {
        $this->addFileQuery = db_insert('bundlecache_file_bundle')->fields(array('bid', 'fid', 'weight'));
        // Make sure that the file is always added to the bundle as last element.
        // If there are no files in the bundle yet, NULL will be casted to 0.
        $weight = (int)db_query('SELECT max(weight) FROM {bundlecache_file_bundle} WHERE bid = :bid', array(':bid' => $this->bid))->fetchField();
      }
      $this->addFileQuery->values(array('bid' => $this->bid, 'fid' => $file->fid, 'weight' => ++$weight));
      $this->files[] = $file->path;
      $this->dirty = TRUE;
    }
  }

  protected $removeFileQuery = NULL;
  protected $removeFileQueryCondition = NULL;
  public function removeFile(Bundlecache_File $file) {
    static $or;
    if (in_array($file->path, $this->files)) {
      if (!isset($this->removeFileQuery)) {
        $or = db_or();
        $this->removeFileQuery = db_delete('bundlecache_file_bundle')->condition($or);
      }
      $or->condition(db_and()->condition('bid', $this->bid)->condition('fid', $file->fid));
      unset($this->files[array_search($file->fid, $this->files)]);
      $this->dirty = TRUE;
    }
  }
}



class Bundlecache_File {
  const LOAD_BIDS = 0x1;
  const LOAD_WEIGHT = 0x2;
  public $fid = NULL;
  public $kind = 'css';
  public $path = '';
  public $hash = '';
  public $size = 0;
  public $bids = array();


  public static function query($flags = NULL) {
    $query = db_select('bundlecache_file', 'f', array('fetch' => 'Bundlecache_File'));
    // Make sure path and size are the first two columns so that we can reference them properly.
    $query->fields('f', array('path', 'size', 'hash', 'kind', 'fid'));

    if ($flags === self::LOAD_BIDS) {
      $query->groupBy('f.fid');
      $query->addExpression("1", 'bids');
    }
    elseif ($flags === self::LOAD_WEIGHT) {
      $query->leftJoin('bundlecache_file_bundle', 'fb', 'f.fid = fb.fid');
      $query->orderBy('fb.weight');
      $query->addExpression('fb.weight', 'weight');
    }

    return $query;
  }

  public static function createRecord($hash, $path) {
    $file = new Bundlecache_File();
    return $file->create($path, $hash);
  }

  public static function updateRecord($hash, $path) {
    $file = Bundlecache_File::query()->condition('path', $path)->execute()->fetch();
    $file->init();
    $file->update($hash)->save();
    return $file;
  }

  public static function deleteRecord($hash, $path) {
    $file = Bundlecache_File::query()->condition('path', $path)->execute()->fetch();
    $file->init();
    return $file->delete();
  }

  public function init() {
    if (isset($this->fid)) {
      // This was loaded from database and is prepopulated.
      $this->fid  = (int)$this->fid;
      $this->size = (int)$this->size;
    }
    if (isset($this->bids) && !is_array($this->bids)) {
      $this->bids = db_query('SELECT fb.bid FROM {bundlecache_file_bundle} fb WHERE fb.fid = :fid', array(':fid' => $this->fid))->fetchCol();
    }
    else {
      $this->bids = array();
    }
  }

  public function dirtify() {
    // This file was changed, so set all bundles to dirty that include it.
    db_query('UPDATE {bundlecache_bundle} SET dirty = 1 WHERE bid IN (SELECT bid FROM {bundlecache_file_bundle} WHERE fid = :fid)', array(':fid' => $this->fid));
  }

  public function delete() {
    // Delete the programmatic bundle associated with this file.
    $query = Bundlecache_Bundle::query();
    $query->leftJoin('bundlecache_file_bundle', 'fb', 'b.bid = fb.bid');
    $bundle = $query->condition('fb.fid', $this->fid)->condition('b.programmatic', TRUE)->execute()->fetch();
    if ($bundle) {
      $bundle->init();
      $bundle->delete();
    }

    $this->dirtify();
    db_delete('bundlecache_file')->condition('fid', $this->fid)->execute();
    db_delete('bundlecache_file_bundle')->condition('fid', $this->fid)->execute();
  }

  public function create($path, $hash = NULL) {
    $this->kind = pathinfo($path, PATHINFO_EXTENSION);
    $this->path = $path;
    $this->update($hash);
    $this->fid = db_insert('bundlecache_file')->fields($this->fields())->execute();

    // Create the programmatic bundle for it.
    $bundle = Bundlecache_Bundle::createRecord(array(
      'title' => $this->path,
      'kind' => $this->kind,
      'programmatic' => TRUE,
      'dirty' => TRUE,
    ));
    $bundle->addFile($this);
    $bundle->save();

    return $this;
  }

  public function update($hash = NULL) {
    $this->hash = isset($hash) ? $hash : md5_file($this->path);
    $this->size = strlen(gzencode(file_get_contents($this->path)));
    return $this;
  }

  public function fields() {
    return array(
      'path' => $this->path,
      'size' => $this->size,
      'hash' => $this->hash,
      'kind' => $this->kind,
    );
  }

  public function save() {
    $result = db_update('bundlecache_file')->condition('fid', $this->fid)->fields($this->fields())->execute();
    $this->dirtify();
    return $result;
  }

  public function url() {
    if ($this->path) {
      return file_create_url($this->path);
    }
    else {
      return FALSE;
    }
  }
}
