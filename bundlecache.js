
Drupal.behaviors.bundlecache = {
  attach: function (context) {
    jQuery('select.bundles', context).once('selectpopup').selectPopup();
    jQuery('.messages .bundlecache-export').select();
  }
};

(function($) {
  $.fn.selectPopup = function() {
    return this.each(function() {
      var element = $(this).wrap('<div class="selectpopup-select"></div>');
      var select = element.parent();
      var list = $('<div class="selectpopup-list"></div>').insertAfter(select);
      var selected = [];

      list.click(function (e) {
        list.css('visibility', 'hidden');
        select.show();
        selected = $('option:selected', element);
        element.focus();
        return false;
      });

      $('option', element).mousedown(function(e) {
        e.preventDefault();
        this.selected = !this.selected;
      });

      element.blur(function (e) {
        select.hide();
        var content = $('option:selected', element).map(function() {
          return '<span>' + $(this).text() + '</span>';
        }).get().join(', ') || '&lt;<span class="placeholder">none</span>&gt;';
        list.html('<a href="#">' + content + '</a>');
        list.css('visibility', 'visible').focus();
      }).keydown(function(e) {
        if (e.keyCode == 27) { // Esc
          // Reselect the old selection.
          selected.attr('selected', true);
          $('option:selected', element).not(selected).attr('selected', false);
          element.blur();
        }
        else if (e.keyCode == 13) { // Enter
          e.preventDefault();
          element.blur();
        }
      }).dblclick(function() {
        element.blur();
      }).blur();
    });
  };
})(jQuery);

(function($) {
  $(function() {
    for (var depth = 12; depth >= 0; depth--) {
      $('.bundlecache-directory.bundlecache-depth-' + depth).each(function() {
        var dir = $(this);
        var hidden;
        $('.path', dir).click(function(e) {
          if (dir.is('.expanded')) {
            hidden = $('.' + dir.attr('id') + ':hidden');
            $('.' + dir.attr('id')).hide();
            dir.removeClass('expanded').addClass('collapsed');
          }
          else {
            $('.' + dir.attr('id')).not(hidden).show();
            dir.addClass('expanded').removeClass('collapsed');
          }
          return false;
        }).click();
        $('a', dir).click(function(e) {  });
      });
    }
  });
})(jQuery);