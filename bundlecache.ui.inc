<?php

/**
 * @file
 * Contains the administrative user interface for this module.
 */

require_once 'bundlecache.api.inc';


function bundlecache_ui_linkify($file) {
  return '<a href="' . file_create_url($file) . '">' . $file . '</a>';
}

function bundlecache_ui_detect_files() {
  list($new, $updated, $deleted) = bundlecache_detect_files();

  if (count($new)) {
    drupal_set_message(format_plural(count($new),
      "Bundlecache detected 1 new file: !list",
      "Bundlecache detected @count new files: !list",
      array('!list' => theme('item_list', array('items' => array_map('bundlecache_ui_linkify', array_keys($new)))))
    ));
  }
  if (count($updated)) {
    drupal_set_message(format_plural(count($updated),
      "Bundlecache detected 1 updated file: !list",
      "Bundlecache detected @count updated files: !list",
      array('!list' => theme('item_list', array('items' => array_map('bundlecache_ui_linkify', array_keys($updated)))))
    ));
  }
  if (count($deleted)) {
    drupal_set_message(format_plural(count($deleted),
      "Bundlecache detected 1 deleted file: !list",
      "Bundlecache detected @count deleted files: !list",
      array('!list' => theme('item_list', array('items' => array_keys($deleted))))
    ));
  }

  if (!$new && !$updated && !$deleted) {
    drupal_set_message(t("There were no changes to any files."));
  }

  bundlecache_clear_caches();
}

function bundlecache_ui_force_recreate() {
  bundlecache_force_recreation();
  bundlecache_ui_update_bundles();
}

function bundlecache_ui_prune_all_data() {
  db_query('DELETE FROM {bundlecache_file}');
  db_query('DELETE FROM {bundlecache_file_bundle}');
  db_query('DELETE FROM {bundlecache_bundle}');
  drupal_set_message(t('Deleted all data.'));
  bundlecache_clear_caches();
}

function bundlecache_ui_update_bundles() {
  try {
    $bundles = bundlecache_update_bundles();

    $regular = array();
    $programmatic = array();
    $protocol = (empty($_SERVER['HTTPS']) ? 'http' : 'https');

    foreach ($bundles as $bundle)
      if ($bundle->programmatic)
        $programmatic[] = '<a href="' . $protocol . ':' . $bundle->url() . '">' . $bundle->title . '</a>';
      else
        $regular[] = '<a href="' . $protocol . ':' . $bundle->url() . '">' . $bundle->title . '</a>';

    if (count($regular)) {
      drupal_set_message(format_plural(count($regular),
        'Bundlecache updated 1 bundle: !list',
        'Bundlecache updated @count bundles: !list',
        array('!list' => theme('item_list', array('items' => $regular)))
      ));
    }
    if (count($programmatic)) {
      drupal_set_message(format_plural(count($programmatic),
        'Bundlecache updated 1 file: !list',
        'Bundlecache updated @count files: !list',
        array('!list' => theme('item_list', array('items' => $programmatic)))
      ));
    }

    if (!($regular || $programmatic)) {
      drupal_set_message(t('There was nothing to update.'));
    }

    bundlecache_clear_caches();
  }
  catch (Exception $e) {
    drupal_set_message(t('An error occured while aggregating the bundle %title: !message', array('%title' => $e->title, '!message' => $e->getMessage())));
  }
}

function bundlecache_ui_files($form, &$form_state, $kind) {
  $form_state['storage']['kind'] = $kind;

  $result = Bundlecache_Bundle::query()->condition('b.programmatic', 0)->condition('b.kind', $kind)->execute();
  $bundles = array();
  $bundle_selection = array();
  foreach ($result as $bundle) {
    $bundle->init();
    $bundles[$bundle->bid] = $bundle;
    $bundle_selection[$bundle->bid] = $bundle->title;
  }

  drupal_add_css(drupal_get_path('module', 'bundlecache') . '/bundlecache.css');
  drupal_add_js(drupal_get_path('module', 'bundlecache') . '/bundlecache.js');

  $form['files'] = array(
    '#type' => 'table',
    '#header' => array(
      'path'    => array('data' => t('Path'), 'width' => '50%'),
      'bundles' => array('data' => t('Bundles'), 'width' => '40%'),
      'size'    => array('data' => t('Size (gzipped)'), 'width' => '10%'),
    ),
    '#rows' => array(),
    '#tree' => TRUE,
    '#attributes' => array('id' => 'bundlecache-file-table'),
    '#empty' => t('There are no files. <a href="@url">Click on detect changes</a> to scan for files.', array('@url' => url('admin/config/development/bundlecache'))),
  );

  $bids = array_keys($bundles);

  $paths = array();

  $files = Bundlecache_File::query(Bundlecache_File::LOAD_BIDS)->condition('f.kind', $kind)->orderBy('f.path')->execute();
  foreach ($files as $file) {
    $file->init();

    $classes = array();
    $directories = explode('/', dirname($file->path));
    $depth = count($directories);
    $path = '';
    $path_depth = -1;
    foreach ($directories as $directory) {
      $path .= ($path ? '/' : '') . $directory;
      $path_depth++;

      if (!isset($paths[$path])) {
        $directory_classes = $classes;
        $directory_classes[] = "bundlecache-depth-$path_depth";
        $directory_classes[] = "bundlecache-directory";
        $directory_classes[] = "expanded";
        $form['files'][$path] = array(
          '#attributes' => array('id' => drupal_html_id("dir-" . str_replace('/', '-', $path)), 'class' => $directory_classes),
          '#:path' => '<div class="path"><a href="#">' . check_plain($directory) . '</a></div>',
        );
        $paths[$path] = TRUE;
      }
      $classes[] = drupal_html_class('dir-' . $path);
    }

    $source = '<small>' . t('(<a href="@url">source</a>)', array('@url' => $file->url())) . '</small>';
    $classes[] = "bundlecache-depth-$depth";
    $form['files'][$file->fid] = array(
      '#attributes' => array('id' => "file-$file->fid", 'class' => $classes),
      '#:path'    => '<div class="path">' . check_plain(basename($file->path)) . ' ' . $source . '</div>',
      'bundles' => array(
        '#type' => 'select',
        '#options' => $bundle_selection,
        '#default_value' => array_intersect($file->bids, $bids),
        '#multiple' => TRUE,
        '#attributes' => array('class' => array('bundles')),
      ),
      '#:size' => bundlecache_ui_size_cell($file->size),
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
    '#submit' => array('bundlecache_ui_files_submit'),
  );


  return $form;
}

function bundlecache_ui_files_submit($form, &$form_state) {
  $result = Bundlecache_Bundle::query()->condition('b.programmatic', 0)->condition('b.kind', $form_state['storage']['kind'])->execute();
  $bundles = array();
  foreach ($result as $bundle) {
    $bundle->init();
    $bundles[$bundle->bid] = $bundle;
  }

  $result = Bundlecache_File::query(Bundlecache_File::LOAD_BIDS)->condition('f.kind', $form_state['storage']['kind'])->orderBy('f.path')->execute();
  $files = array();
  foreach ($result as $file) {
    $file->init();
    $files[$file->fid] = $file;
  }

  foreach ($form_state['values']['files'] as $fid => $file) {
    if (isset($files[$fid])) {
      foreach (array_diff($file['bundles'], $files[$fid]->bids) as $bid) {
        if (isset($bundles[$bid])) {
          $bundles[$bid]->addFile($files[$fid]);
        }
      }
      foreach (array_diff($files[$fid]->bids, $file['bundles']) as $bid) {
        if (isset($bundles[$bid])) {
          $bundles[$bid]->removeFile($files[$fid]);
        }
      }
    }
  }

  foreach ($bundles as $bundle) {
    $bundle->save();
  }

  cache_clear_all('*', 'cache_bundlecache', TRUE);
}

function bundlecache_ui_bundles($form, &$form_state) {
  drupal_add_css(drupal_get_path('module', 'bundlecache') . '/bundlecache.css');
  drupal_add_js(drupal_get_path('module', 'bundlecache') . '/bundlecache.js');

  $form['administration'] = array(
    '#type' => 'fieldset',
    '#title' => t('Administration'),
  );

  $form['administration']['scan'] = array(
    '#type' => 'submit',
    '#value' => t('Detect files'),
    '#submit' => array('bundlecache_ui_detect_files'),
  );

  $dirty = (int)db_query('SELECT COUNT(*) FROM {bundlecache_bundle} WHERE dirty = 1')->fetchField();
  $form['administration']['update'] = array(
    '#type' => 'submit',
    '#value' => t('Update bundles (@dirty)', array('@dirty' => $dirty)),
    '#submit' => array('bundlecache_ui_update_bundles'),
  );

  $header_cell = theme('table_select_header_cell');
  if (!is_array($header_cell)) $header_cell = array();
  $header_cell += array('width' => '1%');

  $form['bundles'] = array(
    '#type' => 'table',
    '#header' => array(
      # 'enabled' => array('data' => '&nbsp;', 'width' => '1%', 'field' => 'b.enabled'),
      'export' => $header_cell,
      'title' => array('data' => t('Title'), 'width' => '60%', 'field' => 'b.title'),
      'kind'  => array('data' => t('Type'), 'width' => '10%', 'field' => 'b.kind', 'sort' => 'asc'),
      'size'  => array('data' => t('Size (gzipped)'), 'width' => '12%', 'field' => 'b.size'),
      'edit,delete' => array('data' => t('Operations'), 'colspan' => 2),
    ),
    '#rows' => array(),
    '#tree' => TRUE,
    '#empty' => t('There are no bundles. <a href="@url">Create one</a>.', array('@url' => url('admin/config/development/bundlecache/bundles/add'))),
  );

  $export = array();

  $result = Bundlecache_Bundle::query()->condition('b.programmatic', 0)->extend('TableSort')->orderByHeader($form['bundles']['#header'])->execute();
  foreach ($result as $bundle) {
    $bundle->init();
    $title = l($bundle->title, "admin/config/development/bundlecache/bundle/$bundle->bid/edit", array('attributes' => array('class' => 'bundlename')));
    $source = ' <small>' . ($bundle->path ? t('(<a href="@url">source</a>)', array('@url' => $bundle->url())) : t('not yet built')) . '</small>';
    $dirty = $bundle->dirty ? '<span class="warning dirty" title="' . t('This bundle is not up-to-date.') . '">*</span>' : '';

    $form['bundles'][$bundle->bid] = array(
      '#attributes' => array('id' => "bundle-$bundle->bid"),
      'export' => array(
        '#type' => 'checkbox',
        '#default_value' => FALSE,
      ),
      # 'enabled' => array(
      #   '#type' => 'checkbox',
      #   '#default_value' => $bundle->enabled,
      # ),
      '#:title'   => $title . $dirty . $source,
      '#:kind'    => $bundle->kind == 'css' ? t('Styles') : t('JavaScript'),
      '#:size' => bundlecache_ui_size_cell($bundle->size),
      '#:edit' => l(t('edit'), "admin/config/development/bundlecache/bundle/$bundle->bid/edit"),
      '#:delete' => l(t('delete'), "admin/config/development/bundlecache/bundle/$bundle->bid/delete"),
    );
  }

  # $form['submit'] = array(
  #   '#type' => 'submit',
  #   '#value' => t('Save changes'),
  #   '#submit' => array('bundlecache_ui_files_submit'),
  # );
  $form['export'] = array(
    '#type' => 'submit',
    '#value' => t('Export selected'),
    '#submit' => array('bundlecache_ui_files_export'),
  );

  return $form;
}

function bundlecache_ui_files_export($form, &$form_state) {
  $bundles = array();
  foreach ($form_state['values']['bundles'] as $bid => $settings) {
    if (!empty($settings['export'])) {
      $bundles[] = $bid;
    }
  }

  if (count($bundles)) {
    $result = Bundlecache_Bundle::query()->condition('b.bid', $bundles, 'IN')->execute();
    $export = array();
    foreach ($result as $bundle) {
      $bundle->init();
      $export[$bundle->kind][$bundle->title] = $bundle->files;
    }

    if (!empty($export)) {
      drupal_set_message('<textarea class="bundlecache-export" readonly="readonly">' . str_replace("\/", "/", json_encode($export)) . '</textarea>');
    }
  }
}

function bundlecache_ui_bundles_add($form, &$form_state) {
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('The name of the bundle.'),
    '#size' => 40,
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  $form['kind'] = array(
    '#type' => 'radios',
    '#title' => t('Type'),
    '#description' => t('The file type the bundle may contain.'),
    '#options' => array('css' => 'CSS files', 'js' => 'JavaScript files'),
    '#default_value' => 'css',
  );

  $form['create'] = array(
    '#type' => 'submit',
    '#value' => t('Create'),
    '#submit' => array('bundlecache_ui_bundles_add_submit'),
  );

  return $form;
}

function bundlecache_ui_bundles_add_validate($form, &$form_state) {
  $existing_title = db_query('SELECT title FROM {bundlecache_bundle} WHERE LOWER(title) = LOWER(:title) AND kind = :kind', array(':title' => $form_state['values']['title'], ':kind' => $form_state['values']['kind']))->fetchField();
  if ($existing_title) {
    form_set_error('title', t('There is already a bundle with that name.'));
  }
}

function bundlecache_ui_bundles_add_submit($form, &$form_state) {
  Bundlecache_Bundle::createRecord(array(
    'title' => $form_state['values']['title'],
    'kind' => $form_state['values']['kind'],
  ));

  $form_state['redirect'] = 'admin/config/development/bundlecache';
  bundlecache_clear_caches();
}

function bundlecache_ui_bundle_delete($form, &$form_state, $bundle) {
  $form_state['storage']['bundle'] = $bundle;
  return confirm_form($form, t('Are you sure you want to delete the bundle %title?', array('%title' => $bundle->title)), 'admin/config/development/bundlecache/bundles', t('This bundle will no longer be available. Files in that bundle are not deleted.'), t('Delete'));
}

function bundlecache_ui_bundle_delete_submit($form, &$form_state) {
  $form_state['storage']['bundle']->delete();
  drupal_set_message(t('The bundle %title has been deleted.', array('%title' => $form_state['storage']['bundle']->title)));
  $form_state['redirect'] = 'admin/config/development/bundlecache';
  bundlecache_clear_caches();
}

function bundlecache_ui_bundle_edit($form, &$form_state, $bundle) {
  $form_state['storage']['bundle'] = $bundle;

  drupal_set_title(t('Bundle %title', array('%title' => $bundle->title)), PASS_THROUGH);
  drupal_add_css(drupal_get_path('module', 'bundlecache') . '/bundlecache.css');
  drupal_add_js(drupal_get_path('module', 'bundlecache') . '/bundlecache.js');

  $form['duplicate'] = array(
    '#type' => 'submit',
    '#value' => t('Duplicate'),
    '#submit' => array('bundlecache_ui_bundle_edit_duplicate'),
  );

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => check_plain($bundle->title),
    '#required' => TRUE,
  );

  $form['files'] = array(
    '#type' => 'table',
    '#header' => array(
      'path'    => array('data' => t('Path'), 'width' => '50%'),
      'size'    => array('data' => t('Size (gzipped)'), 'width' => '10%'),
      'weight'  => array('data' => t('Weight')),
    ),
    '#rows' => array(),
    '#tree' => TRUE,
    '#attributes' => array('id' => 'bundlecache-file-table'),
  );
  drupal_add_tabledrag('bundlecache-file-table', 'order', 'sibling', 'bundlecache-file-weight');

  $files = Bundlecache_File::query(Bundlecache_File::LOAD_WEIGHT)->condition('fb.bid', $bundle->bid)->execute();
  foreach ($files as $file) {
    $file->init();
    $form['files'][$file->fid] = array(
      '#attributes' => array('id' => "file-$file->fid", 'class' => array('draggable')),
      '#:path' => check_plain($file->path) . ' <small>' . t('(<a href="@url">source</a>)', array('@url' => $file->url())) . '</small>',
      '#:size' => bundlecache_ui_size_cell($file->size),
      'weight' => array('#type' => 'textfield', '#default_value' => $file->weight, '#attributes' => array('class' => array('bundlecache-file-weight'))),
    );
  }

  $form['body'] = array(
    '#type' => 'textarea',
    '#title' => t('Export string'),
    '#description' => t('Copy this to replicate this bundle to another server.'),
    '#cols' => 60,
    '#rows' => 3,
    '#default_value' => str_replace("\/", "/", json_encode(array($bundle->kind => array($bundle->title => $bundle->files)))),
    '#attributes' => array('class' => array('bundlecache-export'), 'readonly' => 'readonly'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
  );

  $form['delete'] = array(
    '#type' => 'markup',
    '#markup' => l(t('Delete'), 'admin/config/development/bundlecache/bundle/' . $bundle->bid . '/delete'),
  );

  return $form;
}

function bundlecache_ui_bundles_edit_validate($form, &$form_state) {
  $bundle = $form_state['storage']['bundle'];

  if ($bundle->title != $form_state['values']['title']) {
    $existing_title = db_query('SELECT title FROM {bundlecache_bundle} WHERE LOWER(title) = LOWER(:title) AND kind = :kind', array(':title' => $form_state['values']['title'], ':kind' => $bundle->kind))->fetchField();
    if ($existing_title) {
      form_set_error('title', t('There is already a bundle with that name.'));
    }
  }
}

function bundlecache_ui_bundle_edit_submit($form, &$form_state) {
  $bundle = $form_state['storage']['bundle'];

  $bundle->title = $form_state['values']['title'];
  foreach ($form_state['values']['files'] as $fid => $file) {
    db_query('UPDATE {bundlecache_file_bundle} SET weight = :weight WHERE bid = :bid AND fid = :fid', array(
      'weight' => (int)$file['weight'],
      'bid' => $bundle->bid,
      'fid' => (int)$fid,
    ));
  }
  $bundle->dirty = TRUE;
  $bundle->save();

  drupal_set_message(t('The bundle %title has been updated. Update the bundles to commit the changes.', array('%title' => $form_state['storage']['bundle']->title)));
  $form_state['redirect'] = 'admin/config/development/bundlecache';
  bundlecache_clear_caches();
}

function bundlecache_ui_bundle_edit_duplicate($form, &$form_state) {
  $bundle = $form_state['storage']['bundle'];

  $title = bundlecache_ui_find_unique_name($bundle->kind, t('!title (copy)', array('!title' => $bundle->title)));
  $new = Bundlecache_Bundle::createRecord(array(
    'title' => $title,
    'kind' => $bundle->kind,
    'dirty' => TRUE,
  ));

  db_query('INSERT INTO {bundlecache_file_bundle} SELECT fid, :new_bid, weight FROM {bundlecache_file_bundle} WHERE bid = :old_bid', array(':old_bid' => $bundle->bid, ':new_bid' => $new->bid));
  $form_state['redirect'] = "admin/config/development/bundlecache/bundle/{$new->bid}/edit";
}

function bundlecache_ui_find_unique_name($kind, $title) {
  $original_title = $title;
  $counter = 2;
  while (db_query('SELECT title FROM {bundlecache_bundle} WHERE LOWER(title) = LOWER(:title) AND kind = :kind', array(':title' => $title, ':kind' => $kind))->fetchField()) {
    $title = "$original_title $counter";
    $counter++;
  }
  return $title;
}

function bundlecache_ui_config($form, &$form_state) {
  $form['bundlecache_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#default_value' => variable_get('bundlecache_enabled', TRUE),
    '#description' => t('Uncheck this to disable bundle replacement temporarily without disabling the module itself.')
  );

  $form['paths'] = array(
    '#type' => 'fieldset',
    '#title' => t('Paths'),
  );

  $form['paths']['bundlecache_css_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('CSS file prefix'),
    '#description' => t('The URL where the generated CSS files are accessible. If empty, the default directory is used. Omit leading and trailing slashes. Make sure that the files are available at this URL, e.g. by symlinking or reverse-proxying it to %path. If this path doesn\'t contain a protocol scheme, it will be interpreted as local to the Drupal web root. Bundlecache will automatically switch to https:// when necessary.', array('%path' => 'http:' . file_create_url('bundlecache://css'))),
    '#default_value' => variable_get('bundlecache_css_prefix', ''),
  );

  # $form['paths']['bundlecache_graphics_server'] = array(
  #   '#type' => 'textfield',
  #   '#title' => t('Graphic file server'),
  #   '#description' => t('The URL where CSS graphic files are accessible. If empty, graphics will be loaded local to the server root. When using a custom CSS prefix, make sure that the graphics are available on that host if this field is empty.'),
  #   '#default_value' => variable_get('bundlecache_graphics_server', ''),
  #   '#field_prefix' => 'http(s)://',
  # );

  $form['paths']['bundlecache_js_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('JavaScript file prefix'),
    '#description' => t('The URL where the generated JavaScript files are accessible. If empty, the default directory is used. Omit leading and trailing slashes. Make sure that the files are available at this URL, e.g. by symlinking or reverse-proxying it to %path. If this path doesn\'t contain a protocol scheme, it will be interpreted as local to the Drupal web root. Bundlecache will automatically switch to https:// when necessary.', array('%path' => 'http:' . file_create_url('bundlecache://js'))),
    '#default_value' => variable_get('bundlecache_js_prefix', ''),
  );

  $form['#submit'] = array('bundlecache_clear_caches');

  $form['misc'] = array(
    '#type' => 'fieldset',
    '#title' => t('Miscellaneous options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#access' => user_access('administer bundlecache'),
    '#weight' => 10000,
  );

  $form['misc']['recreate'] = array(
    '#type' => 'submit',
    '#value' => t('Force recreation'),
    '#submit' => array('bundlecache_ui_force_recreate'),
  );

  $form['misc']['prune_all'] = array(
    '#type' => 'submit',
    '#value' => t('Prune ALL data'),
    '#submit' => array('bundlecache_ui_prune_all_data'),
  );

  return system_settings_form($form);
}

/**
 * Returns a color code matching the file size.
 *
 * @param $size
 *   The file size in bytes.
 * @return
 *   A code from 1 to 6 depending on the file size.
 */
function bundlecache_ui_size_cell($size) {
      if ($size <= 512)   $class = 1;
  elseif ($size <= 2048)  $class = 2;
  elseif ($size <= 8192)  $class = 3;
  elseif ($size <= 16384) $class = 4;
  elseif ($size <= 49152) $class = 5;
  else                    $class = 6;

  return array('data' => format_size($size), 'class' => array("bundlecache-color-$class"));
}

function bundlecache_ui_import($form, &$form_state) {
  drupal_add_css(drupal_get_path('module', 'bundlecache') . '/bundlecache.css');

  $form['data'] = array(
    '#type' => 'textarea',
    '#title' => t('Exported string'),
    '#description' => t('Copy and paste the bundle definition from another website into this field to replicate the configuration on this server.'),
    '#cols' => 60,
    '#rows' => 5,
    '#attributes' => array('class' => array('bundlecache-export')),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
  );

  return $form;
}

function bundlecache_ui_import_validate($form, &$form_state) {
  $form_state['storage']['decoded'] = json_decode($form_state['values']['data'], TRUE);
  if (!$form_state['storage']['decoded']) {
    form_set_error('data', t('The data string is invalid.'));
  }
}

function bundlecache_ui_import_submit($form, &$form_state) {
  $data = $form_state['storage']['decoded'];
  list($ignored, $names) = bundlecache_batch_create($data);

  if (!empty($names)) {
    drupal_set_message(format_plural(
      count($names),
      'Imported 1 new bundle: !list',
      'Imported @count new bundles: !list',
      array('!list' => theme('item_list', array('items' => $names)))
    ));

    if (!empty($ignored)) {
      drupal_set_message(format_plural(
        count($ignored),
        '1 file was ignored because it didn\'t exist: !list',
        '@count files were ignored because they didn\'t exist: !list',
        array('!list' => theme('item_list', array('items' => array_unique($ignored))))
      ));
    }

    $form_state['redirect'] = 'admin/config/development/bundlecache';
  }
  else {
    drupal_set_message(t('There was nothing to import.'));
  }
}
