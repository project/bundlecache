<?php

/**
 * @file
 * Contains the menu definition for this module.
 */

$items['admin/config/development/bundlecache'] = array(
  'title' => 'Bundlecache',
  'description' => t('Intelligently aggregate stylesheets and JavaScript files for faster delivery.'),
  'page callback' => 'drupal_get_form',
  'page arguments' => array('bundlecache_ui_bundles'),
  'file' => 'bundlecache.ui.inc',
  'access arguments' => array('administer bundlecache'),
);

$items['admin/config/development/bundlecache/bundles'] = array(
  'title' => 'Bundles',
  'type' => MENU_DEFAULT_LOCAL_TASK,
  'weight' => -10,
);

$items['admin/config/development/bundlecache/bundles/add'] = array(
  'title' => 'Add bundle',
  'page callback' => 'drupal_get_form',
  'page arguments' => array('bundlecache_ui_bundles_add'),
  'file' => 'bundlecache.ui.inc',
  'access arguments' => array('administer users'),
  'type' => MENU_LOCAL_ACTION,
);

$items['admin/config/development/bundlecache/bundle/%bundlecache_bundle/delete'] = array(
  'title' => 'Delete bundle',
  'page callback' => 'drupal_get_form',
  'page arguments' => array('bundlecache_ui_bundle_delete', 5),
  'file' => 'bundlecache.ui.inc',
  'access arguments' => array('administer bundlecache'),
  'type' => MENU_CALLBACK,
);

$items['admin/config/development/bundlecache/bundle/%bundlecache_bundle/edit'] = array(
  'title' => 'Edit bundle',
  'page callback' => 'drupal_get_form',
  'page arguments' => array('bundlecache_ui_bundle_edit', 5),
  'file' => 'bundlecache.ui.inc',
  'access arguments' => array('administer bundlecache'),
  'type' => MENU_CALLBACK,
);

$items['admin/config/development/bundlecache/files'] = array(
  'title' => 'Files',
  'page callback' => 'drupal_get_form',
  'page arguments' => array('bundlecache_ui_files', 'css'),
  'file' => 'bundlecache.ui.inc',
  'access arguments' => array('administer bundlecache'),
  'type' => MENU_LOCAL_TASK,
  'weight' => 0,
);

$items['admin/config/development/bundlecache/files/css'] = array(
  'title' => 'Styles',
  'type' => MENU_DEFAULT_LOCAL_TASK,
  'weight' => -10,
);

$items['admin/config/development/bundlecache/files/js'] = array(
  'title' => 'JavaScript',
  'page callback' => 'drupal_get_form',
  'page arguments' => array('bundlecache_ui_files', 'js'),
  'file' => 'bundlecache.ui.inc',
  'access arguments' => array('administer bundlecache'),
  'type' => MENU_LOCAL_TASK,
  'weight' => 0,
);

$items['admin/config/development/bundlecache/config'] = array(
  'title' => 'Configuration',
  'page callback' => 'drupal_get_form',
  'page arguments' => array('bundlecache_ui_config'),
  'file' => 'bundlecache.ui.inc',
  'access arguments' => array('administer bundlecache'),
  'type' => MENU_LOCAL_TASK,
  'weight' => 30,
);

$items['admin/config/development/bundlecache/import'] = array(
  'title' => 'Import',
  'page callback' => 'drupal_get_form',
  'page arguments' => array('bundlecache_ui_import'),
  'file' => 'bundlecache.ui.inc',
  'access arguments' => array('administer bundlecache'),
  'type' => MENU_LOCAL_TASK,
  'weight' => 20,
);
