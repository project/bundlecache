<?php

/**
 * Implementation of hook_drush_command().
 */
function bundlecache_drush_command() {
  $items = array();

  $items['bundlecache-detect-files'] = array(
    'description' => "Detects new CSS and JavaScript files.",
    'examples' => array('drush bundlecache-detect-files'),
    'aliases' => array('bcdf'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  $items['bundlecache-update-bundles'] = array(
    'description' => "Updates minified and aggregated files and bundles.",
    'examples' => array('drush bundlecache-update-bundles'),
    'aliases' => array('bcub'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  return $items;
}

function drush_bundlecache_detect_files() {
  module_load_include('inc', 'bundlecache', 'bundlecache.api');

  list($new, $updated, $deleted) = bundlecache_detect_files();
  bundlecache_clear_caches();

  if (count($new)) {
    drush_print(format_plural(count($new), "Bundlecache detected 1 new file:", "Bundlecache detected @count new files:"));
    drush_print("- " . implode("\n- ", array_keys($new)) . "\n");
  }
  if (count($updated)) {
    drush_print(format_plural(count($updated), "Bundlecache detected 1 updated file:", "Bundlecache detected @count updated files:"));
    drush_print("- " . implode("\n- ", array_keys($updated)) . "\n");
  }
  if (count($deleted)) {
    drush_print(format_plural(count($deleted), "Bundlecache detected 1 deleted file:", "Bundlecache detected @count deleted files:"));
    drush_print("- " . implode("\n- ", array_keys($deleted)) . "\n");
  }

  if (!$new && !$updated && !$deleted) {
    drush_print(dt("There were no changes to any files.") . "\n");
  }
}

function drush_bundlecache_update_bundles() {
  module_load_include('inc', 'bundlecache', 'bundlecache.api');

  try {
    $bundles = bundlecache_update_bundles();
    bundlecache_clear_caches();

    $regular = array();
    $programmatic = array();

    foreach ($bundles as $bundle)
      if ($bundle->programmatic)
        $programmatic[] = $bundle->title;
      else
        $regular[] = $bundle->title;

    if (count($regular)) {
      drush_print(format_plural(count($regular), "Bundlecache updated 1 bundle:", "Bundlecache updated @count bundles:"));
      drush_print("- " . implode("\n- ", $regular) . "\n");
    }
    if (count($programmatic)) {
      drush_print(format_plural(count($programmatic), "Bundlecache updated 1 file:", "Bundlecache updated @count files:"));
      drush_print("- " . implode("\n- ", $programmatic) . "\n");
    }

    if (!($regular || $programmatic)) {
      drush_print(dt('There was nothing to update.') . "\n");
    }

  }
  catch (Exception $e) {
    drush_print(dt('An error occured while aggregating the bundle %title: !message', array('%title' => $e->title, '!message' => $e->getMessage())) . "\n");
  }
}